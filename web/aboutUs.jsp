<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TriviaGame for Masters</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="aboutUs.css">
    </head>
    <body>
        <div id="bigDiv">
            <h1>About Trivia For Masters</h1>
            <h3 style="text-align: center">
                Trivia For Masters is designed and made by Dani Livshits and Lena Knyazhansky.<br />
                We created this site in 2014 and after several revisions, it currently has about 100 different questions.
            </h3>


            <div id="Developers">
                <h3>Lena Knyazhansky</h3>
                <center>
                    <img src="Images/Lena.jpg" alt="Logo"><br />
                    <a href="mailto:LenaKn85@gmail.com" target="_top" style="color: white;">Contact Lena</a>
                </center>   
            </div>

            <div id="Developers">
                <h3>Dani Livshits</h3>
                <center>
                    <img src="Images/Dani.jpg" alt="Logo" style="height: 115px;"><br />
                    <a href="mailto:tgeller1@walla.com" target="_top" style="color: white;">Contact Dani</a>
                </center>
                <div>

                </div>
            </div>

        </div>
    </body>
</html>

