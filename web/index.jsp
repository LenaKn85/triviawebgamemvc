
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>TriviaGame for Masters</title>
        <link rel="stylesheet" href="mainStyle.css" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script type="text/javascript">

            function hide_start_button() {/* when page onload first time" */

                document.getElementById("start").style.visibility = 'hidden';
                document.getElementById("user_back").style.visibility = 'hidden';
                /*check if there were cookies before*/
                /*document.location.href="WelcomeServlet";*/


                var cookieArr = document.cookie.split(';');
                if (cookieArr !== null) {
                    for (var i = 0; i < cookieArr.length; i++) {
                        name = cookieArr[i].split('=')[0];
                        value = cookieArr[i].split('=')[1];
                        if (name === "repeatVisitor" && value === "yes") {
                            /*hide login form  */

                            document.getElementById("login_form").style.visibility = 'hidden';
                            document.getElementById("span_password").style.visibility = 'hidden';
                            document.getElementById("start").style.visibility = 'visible';
                            document.getElementById("user_back").style.visibility = 'visible';
                        }
                        /* alert("Key is : " + name + " and Value is : " + value);*/
                    }
                }
            }

            function  hide_picture() {
            }

            function check_login() {
                var login = document.getElementById("usr").value;
                var pass = document.getElementById("pass").value;
                var count_error_login_pass = 0;

                if (login || pass) {
                    if (/^[a-zA-Z1-9]+$/.test(login) === false) {
                        count_error_login = 1;
                        alert("Username must be only latin letters");
                        return false;
                    }
                    if (login.length < 4 || login.length > 20) {
                        count_error_login = 1;
                        alert('username"s length is 4-20 symbols');
                        return false;
                    }
                    if (parseInt(login.substr(0, 1))) {
                        count_error_login = 1;
                        alert('Username must strart with a letter');
                        return false;
                    }
                    var r = /[^A-Z-a-z-0-9]/g;
                    if (r.test(pass)) {
                        count_error_login = 1;
                        alert("Password need include only latin letters");
                        return false;
                    }
                    if (pass.length < 6) {
                        count_error_login = 1;
                        alert("Password must be longer than 6 letters");
                        return false;
                    }
                    if (pass.length > 20) {
                        count_error_login = 1;
                        alert("Password is longer than 20 letters");
                        return false;
                    }

                }//if null
                else {
                    alert("Please Enter Username and Password");
                    return false;
                }
                if (count_error_login_pass == 0) {/*we can put User Name on Screen*/

                    var logn = document.getElementById("login_form");

                    logn.action = "WelcomeServlet"; /* Servlet will if we need Coockie for user adn remember him in future */
                    logn.target = "iFrame";
                    document.getElementById("login_form").style.visibility = 'hidden';
                    document.getElementById("span_password").style.visibility = 'hidden';

                    document.getElementById("start").style.visibility = 'visible';

                }
            }
        </script>
    </head>

    <body onload="hide_start_button();">
        <div id="outerFrame">

            <div id="header">
                <center>
                    <img src="Images/triviaLogo.png" alt="Logo" style="width:390px;height:162px">
                </center>
            </div>

            <div id="nav">
                <span style="font-family:Monotype Corsiva;font-size:22px;color:red;" id="span_password">Enter your name and password</span>

                <form method="post" id="login_form" name="loginform" action="LoginCntrl"> 
                    <p style="color:white">
                        <span style=";font-family:Monotype Corsiva;font-size:22px;color:blue;">Username:</span>
                        <input type="text"  id="usr" class="old" size="9" name="username"><br>

                    <p style="color:white"> <span style=";font-family:Monotype Corsiva;font-size:22px;color:blue;">Password:</span>
                        <input type="text" id="pass" class="old" size="10" name="password"><br>
                        <label>
                            <span style=";font-family:Monotype Corsiva;font-size:22px;color:blue;">remember me</span>
                        </label>
                        <input type="checkbox" name="checkBoxLogin"  id="checkBox">
                        <br>
                        <input id=submit type="submit" class="b1" value="login" onclick="check_login()">
                        <br>
                </form>

                <h3>
                    <span style="font-family:Monotype Corsiva;font-size:22px;color:red;" id="user_back">Welcome back!!!</span>
                </h3>
                <h3>
                    <a href="categories" id="start"  target="iFrame">
                        <span style="font-family:Monotype Corsiva;font-size:22px;color:whitesmoke;"></span>START THE GAME</a>
                </h3>
                <h3>
                    <a href="aboutUs.jsp" onclick="hide_picture()" target="iFrame">
                        <span style="font-family:Monotype Corsiva;font-size:22px;color:blue;">About Us</span>
                    </a>
                </h3>
                <h3>
                    <a href="onlineGames.jsp" target="iFrame">
                        <span style=";font-family:Monotype Corsiva;font-size:22px;color:blue;">Other Trivia Games</span>
                    </a>
                </h3>
            </div>

            <div id="section">
                <iframe src="aboutUs.jsp" name="iFrame" style="width: 100%; height: 100%;"> 
                </iframe>       
            </div>


            <div id="footer">
                Copyright � 2014 Lena & Dani
            </div>

        </div>
    </body>
</html>