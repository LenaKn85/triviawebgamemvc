<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TriviaGame for Masters</title>
        <link rel="stylesheet" href="iFramebody.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">

        <style type="text/css">
            div {text-align: center; font-family: Monotype Corsiva; color:whitesmoke;}
            span {font-size:32px;}
        </style>
    </head>

    <body>
        <div>
            <p style="font-size: 50px;">Other Online Trivia Games</p>

            <p><a href="http://www.funtrivia.com/" target="_blank"><span><b>Fun Trivia</b></span></a></p>

            <p><a href="http://www.triviaplaza.com/" target="_blank"><span><b>Trivia Plaza</span></h3></b></a></p>

            <p><a href="http://www.queendom.com/tests/testscontrol.htm?t=2" target="_blank"><span><b>Queendom</b></span></a>

            <p><a href="http://www.youplay.com/games/view/trivia-buzz/" target="_blank"><span><b>YouPlay</b></span></a></p>
        </div>
    </body>
</html>