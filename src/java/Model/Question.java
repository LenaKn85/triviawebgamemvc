package Model;

public class Question {

    private int questionid;
    private String questioncontent;
    private int questiontype;
    private int questiondifficulty;
    private int category;
    private int Answer;

    
    public int getQuestionid() {
        return questionid;
    }

    public void setQuestionid(int questionid) {
        this.questionid = questionid;
    }

    public String getQuestioncontent() {
        return questioncontent;
    }

    public void setQuestioncontent(String questioncontent) {
        this.questioncontent = questioncontent;
    }

    public int getQuestiontype() {
        return questiontype;
    }

    public void setQuestiontype(int questiontype) {
        this.questiontype = questiontype;
    }

    public int getQuestiondifficulty() {
        return questiondifficulty;
    }

    public void setQuestiondifficulty(int questiondifficulty) {
        this.questiondifficulty = questiondifficulty;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getAnswer() {
        return Answer;
    }

    public void setAnswer(int Answer) {
        this.Answer = Answer;
    }
}
