package Model;

public class YesNoAnswer extends Answer {

    private String Answer1;
    private String Answer2;

    public String getAnswer1() {
        return Answer1;
    }

    public void setAnswer1(String Answer1) {
        this.Answer1 = "Yes";
    }

    public String getAnswer2() {
        return Answer2;
    }

    public void setAnswer2(String Answer2) {
        this.Answer2 = "No";
    }
}
