package Model;

public class Answer {

    private int quesanswerid;
    private String correctanswer;

    public int getQuesanswerid() {
        return quesanswerid;
    }

    public void setQuesanswerid(int quesanswerid) {
        this.quesanswerid = quesanswerid;
    }

    public String getCorrectanswer() {
        return correctanswer;
    }

    public void setCorrectanswer(String correctanswer) {
        this.correctanswer = correctanswer;
    }
}
